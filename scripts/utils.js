const fs = require('fs');
const path = require('path');

exports.setDescriptorUrl = (baseUrl) => {
  const configPath = path.join(__dirname, '..', 'public', 'atlassian-connect.json');
  fs.readFile(configPath, (err, data) => {

    const descriptor = JSON.parse(data);
    descriptor.baseUrl = baseUrl;
    fs.writeFile(configPath, JSON.stringify(descriptor, null, 2), (err, result) => {
      if (err) {
        console.error(err);
        process.exit(1);
      }
    });
  });
};
