const ngrok = require('ngrok');
const cp = require('copy-paste');
const utils = require('./utils');

console.log('starting ngrok...');

ngrok.connect(3000).then((url, err) => {
  console.log(`ngrok started on ${url}`);
  utils.setDescriptorUrl(url);
  cp.copy(`${url}/atlassian-connect.json`);
});
